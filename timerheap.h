#ifndef MIN_HEAP_TIMER_H
#define MIN_HEAP_TIMER_H

#include <iostream>
#include <time.h>
#include <netinet/in.h>

using std::exception;

#define BUFFER_SIZE 64 

class heapTimer ;

typedef struct clientData
{
	struct sockaddr_in address;
	int sockfd ;
	char buf[BUFFER_SIZE];
	heapTimer *timer ;
}ClientData_t ;

class heapTimer
{
	public:
		heapTimer(int delay)
		{
			expire = time(NULL) + delay ;
		}
	public:
		time_t expire ;
		void (*cbFunc)(ClientData_t*) ;
		ClientData_t *userData ;
} ;

class timerHeap
{
	public :
		timerHeap(int cap) throw (std::exception):capacity(cap),curSize(0)
		{
			array = new heapTimer*[capacity] ;
			if(!array)
			{
				throw std::exception();
			}
			for(int i = 0 ;i<capacity ;i++)
			{
				array[i] = NULL;
			}
		}

		timerHeap(heapTimer ** initArray,int size,int capacity) throw(std::exception):curSize(size),capacity(capacity)
		{
			if(capacity < size)
			{
				throw std::exception();
			}

			array = new heapTimer*[capacity] ;
			if(!array)
			{
				throw std::exception() ;
			}
			for(int i = 0 ;i< capacity;i++)
			{
				array[i] = NULL ;
			}
			if( size != 0 )
			{
				for(int i = 0 ;i< size;i++)
				{
					array[i] = initArray[i] ;
				}
				for( int i = (curSize-1)/2 ;i>=0 ;--i)
				{
					down(i) ;
				}

			}// size!=0

		}// timerHeap(...)
		
		~timerHeap()
		{
			for(int i = 0 ;i< curSize ;i++)
			{
				delete array[i] ;
			}

			delete[] array;
		}

		bool empty()const {return curSize==0;}

		void addTimer(heapTimer *timer) throw (std::exception)
		{
			if(!timer)
			{
				return ;
			}
	
			if(curSize >= capacity)
			{
				resize() ;
			}
			
			int hole = curSize++;
			int parent = 0 ;
			for(;hole>0; hole=parent)
			{
				parent = (hole-1)/2 ;
				if(array[parent]->expire <= timer->expire)
				{
					break ;
				}
				array[hole] = array[parent];
			}
			array[hole] = timer;
			std::cout<<"hole:"<<hole<<" expire:"<<array[hole]->expire<<std::endl;
		}
		
		void delTimer(heapTimer* timer)
		{
			if(!timer)
			{
				return ;
			}

			timer->cbFunc = NULL;
		}

		heapTimer* top() const
		{
			if(empty())
			{
				return NULL;
			}

			return array[0] ;
		}
		
		void popTimer()
		{
			if(empty())
			{
				return ;
			}
		
			if(array[0])
			{
				std::cout<<"expire:"<<array[0]->expire<<std::endl;
				delete array[0];
				array[0] = array[--curSize];
				down(0);
			}
		}
		
		void tick()
		{
			heapTimer *tmp = array[0] ;
			time_t cur = time(NULL);
			while(!empty())
			{
				if(!tmp)
				{
					break ;
				}

				if(tmp->expire > cur)
				{
					break ;
				}

				if(array[0]->cbFunc)
				{
					array[0]->cbFunc(array[0]->userData);
				}

				popTimer();
				tmp = array[0] ;
			}
		}

	private:
		void down(int hole)
		{
			heapTimer * tmp = array[hole] ;
			int child =hole*2+1  ;
			int min = hole;
			if(child< curSize)
			{
				
				if( child < curSize && (array[min]->expire > array[child]->expire))
				{
					min = child ;
				}

				child = hole*2+2 ;
				if( child < (curSize) && (array[min]->expire > array[child]->expire))
				{
					min = child ;
				}
				
				if(min != hole)
				{
				
					std::cout<<" hole:"<<hole<<" value:"<<array[hole]->expire<<" min:"<<min<<" value:"<<array[min]->expire<<std::endl;
					array[hole]=array[min];
					array[min]=tmp ;
					down(min);
				}
				else
				{
					std::cout<<"hole:"<<hole<<" min:"<<min<<std::endl;
					
				}
			}
		}
		
		void resize() throw(std::exception)
		{
			heapTimer ** tmp = new heapTimer*[2*capacity] ;
			if(!tmp)
			{
				throw std::exception();
			}
			for(int i = 0 ;i< 2*capacity ;i++)
			{
				tmp[i] = NULL;
			}

			capacity = 2*capacity ;
			for(int i = 0 ;i< curSize;i++)
			{
				tmp[i] = array[i] ;
			}

			delete[] array ;
			array = tmp ;
			
		}

	private:
		heapTimer **array ;
		int capacity ;
		int curSize ;
};
#endif
