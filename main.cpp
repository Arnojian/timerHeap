#include <iostream>
#include <time.h>

#include "timerheap.h"


int main()
{
	timerHeap *pTimerHeap = new timerHeap(10) ;
    heapTimer *timer = new heapTimer(1) ;

	pTimerHeap->addTimer(timer) ;
	timer = new heapTimer(2) ;
	pTimerHeap->addTimer(timer) ;
	timer = new heapTimer(3) ;
	pTimerHeap->addTimer(timer) ;
	timer = new heapTimer(4) ;
	pTimerHeap->addTimer(timer) ;
	timer = new heapTimer(5) ;
	pTimerHeap->addTimer(timer) ;
   
	timer = new heapTimer(6) ;
	pTimerHeap->addTimer(timer);
	timer = new heapTimer(7) ;
	pTimerHeap->addTimer(timer) ;
	
	int i = 0 ;
	while(i<10)
	{
		struct timeval tv ;
		tv.tv_sec = 1 ;
		tv.tv_usec = 0 ;
		int ret = select(0,NULL,NULL,NULL,&tv) ;
		if(ret < 0)
		{
			std::cout<<"ret < 0"<< std::endl;
			return -1 ;
		}
		else if (ret ==0)
		{
			pTimerHeap->tick();
			i++;
		}
		else
		{
			std::cout<<"ret > 0"<<std::endl;
		}
	}

	return 0 ;
}
