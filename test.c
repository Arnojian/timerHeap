#include <stdio.h>

void adjustHeap(int *list,int len,int hole)
{
	for(;2*hole+1< len;)
	{
		int tmp = list[hole] ;
		int child = 2*hole+1 ;
		int min = hole;
		if(child < len && list[child] < list[min])
		{
			min = child ;
		}

		child = 2*hole+2 ;
		if(child < len && list[child] < list[min])
		{
			min = child;
		}

		if(min != hole)
		{
			list[hole]=list[min];
			list[min]=tmp;
			hole=min;
		}
		else
		{
			break ;
		}
	}
}

int buildHeap(int *list,int len)
{
	int i = 0 ;

	for(i=len/2;i>=1;i--)
	{
		adjustHeap(list,len,i-1) ;
	}

	return 0 ;
}

int main()
{
	int i =0 ;
	int array[] = {10,9,8,7,6,5,4,3,2,1,0};
	buildHeap(array,sizeof(array)/sizeof(array[0])) ;

	for(i = 0 ;i< sizeof(array)/sizeof(array[0]);i++)
	{
		printf("array hole:%d,value:%d\n",i,array[i]);
	}

	return 0 ;
}
